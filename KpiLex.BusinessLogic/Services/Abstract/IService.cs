﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace KpiLex.BusinessLogic.Services.Abstract
{
    public interface IService<T>
    {
        Task AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task RemoveAsync(T entity);
        Task<IList<T>> GetAll();
    }
}