﻿using KpiLex.Domain.Models;

namespace KpiLex.BusinessLogic.Services.Abstract
{
    public interface IWatchLaterService : IService<WatchLater>
    {
    }
}