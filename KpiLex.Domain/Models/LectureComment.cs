﻿namespace KpiLex.Domain.Models
{
    public class LectureComment
    {
        public int LectureCommentId { get; set; }
        public string Content { get; set; }
        public Lecture Lecture { get; set; }
    }
}