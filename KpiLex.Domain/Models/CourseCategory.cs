﻿using System.Collections.Generic;

namespace KpiLex.Domain.Models
{
    public class CourseCategory
    {
        public int CourseCategoryId { get; set; }
        public string Name { get; set; }
    }
}